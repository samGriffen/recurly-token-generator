import {
  Component,
  OnInit,
  AfterViewInit,
  Output,
  EventEmitter,
} from "@angular/core";
import { environment } from "../../environments/environment";

declare var recurly;

interface iErrDetails {
  field: string;
  message: string[];
}

interface iTokenResult {
  type: string;
  id: string;
}

@Component({
  selector: "app-recurly-token-generator",
  templateUrl: "./recurly-token-generator.component.html",
  styleUrls: ["./recurly-token-generator.component.css"],
})
export class RecurlyTokenGeneratorComponent implements OnInit, AfterViewInit {
  recurlyFormId: string = "recurly-js-sample-combined-card-form";
  errDetails: iErrDetails[] = [];
  @Output() newToken: EventEmitter<string> = new EventEmitter<string>();
  constructor() {}

  ngOnInit() {}

  ngAfterViewInit(): void {
    this.initRecurly();
    // listen for recurly form submit
    document.getElementById(
      this.recurlyFormId
    ).onsubmit = this.recurlyFormSubmit.bind(this);
  }

  initRecurly(): void {
    // init recurly
    recurly.configure({
      publicKey: environment.recurlyPublicKey,
    });
  }

  recurlyFormSubmit(ev: Event): void {
    let recurlyForm = document.getElementById(this.recurlyFormId);
    ev.preventDefault();
    recurly.token(recurlyForm, (err, token: iTokenResult) => {
      if (err) {
        console.log(err);
        // handle error using err.code and err.fields
        this.errDetails = err.details.map((e: iErrDetails) => {
          e.field = e.field.replace("_", " ");
          return e;
        });
      } else {
        console.log(token);
        this.errDetails = [].concat([]);
        this.newToken.next(token.id);
      }
    });
  }

  // helpers
  removeErr(idx: number): void {
    this.errDetails = this.errDetails.filter((e, i) => i !== idx);
  }
}
