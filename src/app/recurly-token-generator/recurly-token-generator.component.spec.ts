import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecurlyTokenGeneratorComponent } from './recurly-token-generator.component';

describe('RecurlyTokenGeneratorComponent', () => {
  let component: RecurlyTokenGeneratorComponent;
  let fixture: ComponentFixture<RecurlyTokenGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecurlyTokenGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecurlyTokenGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
