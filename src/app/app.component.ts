import { Component } from "@angular/core";
declare var axios;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  token: string = "";

  constructor() {}

  onNewToken(token: string): void {
    this.token = token;
    // send to server
    // axios.post("https://api-v2.annuitycheck.com/charge", {
    //   token: token,
    // });
  }
}
