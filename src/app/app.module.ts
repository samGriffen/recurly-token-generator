import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { RecurlyTokenGeneratorComponent } from "./recurly-token-generator/recurly-token-generator.component";

@NgModule({
  declarations: [AppComponent, RecurlyTokenGeneratorComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
